﻿namespace DotNetGetSetInterface
{
    public interface IAnimal
    {
        void MakeSound();
    }
}
