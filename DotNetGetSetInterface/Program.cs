﻿using System;

namespace DotNetGetSetInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            //Feeding Part
            IAnimal animal;
            IMammal mammal;
            string input = string.Empty;

            while (input != "q")
            {
                input = Console.ReadLine();
                if (input == "c")
                {
                    animal = new Cat();
                }
                else if (input == "d")
                {
                    animal = new Dog();
                }
                else if (input == "f")
                {
                    animal = new Fish();
                }
                else 
                {
                    break;
                }
                animal.MakeSound();
            }
            input = string.Empty;
            while (input != "q")
            {
                input = Console.ReadLine();
                if (input == "c")
                {
                    mammal = new Cat();
                }
                else if (input == "d")
                {
                    mammal = new Dog();
                }
                else
                {
                    break;
                }
                mammal.FeedYourBaby();
            }
        }

        public void FirstPart()
        {
            IAnimal animal;
            string input = string.Empty;

            while (input != "q")
            {
                input = Console.ReadLine();
                if (input == "c")
                {
                    animal = new Cat();
                }
                else if (input == "d")
                {
                    animal = new Dog();
                }
                else
                {
                    animal = new Cat();
                }
                animal.MakeSound();
            }
            IAnimal[] array =
            {
                new Cat(),
                new Cat(),
                new Cat(),
                new Dog(),
                new Dog(),
                new Dog(),
                new Dog(),
                new Dog()
            };
            for (int i = 0; i < array.Length; i++)
            {
                array[i].MakeSound();
            }
            Console.ReadLine();
        }
    }
}
