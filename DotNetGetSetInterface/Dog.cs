﻿using System;

namespace DotNetGetSetInterface
{
    public class Dog : IAnimal, IMammal
    {
        public void FeedYourBaby()
        {
            Console.WriteLine("It's fed");
        }

        //private bool _isDead;

        //private int _age;

        //public int Age
        //{
        //    //get => _age;
        //    get
        //    {
        //        return _age;
        //    }
        //    set
        //    {
        //        if (!_isDead)
        //        {
        //            _age = value;
        //        }
        //        else
        //        {
        //            _age = 0;
        //        }
        //    }
        //}
        public void MakeSound()
        {
            Console.WriteLine("Hau");
        }
    }
}
