﻿using System;

namespace DotNetGetSetInterface
{
    public class Cat : IAnimal, IMammal
    {
        public void FeedYourBaby()
        {
            Console.WriteLine("It's fed");
        }

        public void MakeSound()
        {
            Console.WriteLine("Miau");
        }
    }
}
